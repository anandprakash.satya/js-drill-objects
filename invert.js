const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

function invert(obj) {
    const newObj={}
    Object.keys(obj).forEach((key)=>{
        newObj[obj[key]] = key
    })
    return newObj
}
// console.log(invert(testObject));
module.exports = invert