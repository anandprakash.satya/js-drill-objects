const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

    // Like map for arrays, but for objects. Transform the value of each property in turn by passing 
    //it to the callback function.

function mapObject(obj,cb) {
    for(let i=0;i<obj.length;i++){
        Object.keys(obj).forEach((keys)=>{
            obj[keys] = cb(obj[keys])
        })
    }
    return obj
}
// console.log(mapObject(testObject))

module.exports = mapObject