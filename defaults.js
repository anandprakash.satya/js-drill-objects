const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

 // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    
const defaults = (obj,defaultProps) => {
    for(let key in defaultProps){
        if(!(obj.hasOwnProperty(key))){
        obj[key] = defaultProps[key]
        }
    }
    return obj
}
// console.log(defaults(testObject))
module.exports = defaults